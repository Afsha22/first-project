<?php

/*
 * |--------------------------------------------------------------------------
 * | Web Routes
 * |--------------------------------------------------------------------------
 * |
 * | Here is where you can register web routes for your application. These
 * | routes are loaded by the RouteServiceProvider within a group which
 * | contains the "web" middleware group. Now create something great!
 * |
 */
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
/*
 * Route::get('/add-blogger', function () {
 * return view('add-blogger');
 * });
 */

Route::get('/add-blogger', array(
    'as' => 'add-blogger',
    'uses' => 'HomeController@getArticlesData'
));
Route::post('/get-new-blogger', array(
    'as' => 'get-new-blogger',
    'uses' => 'HomeController@insertNewBlogger'
));
        