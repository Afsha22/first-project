@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                 
                </div>
                <div>
                
               
                <table>
                <tr>
                <td>Name</td>
                <td>Articles</td>
                </tr>
                  @foreach($data as $value)
                <tr>
                <td>{{$value->name}}</td>
                <td> <?php
                $artilceName='';
if($value->bloggerhasarticle ){
    foreach($value->bloggerhasarticle as $k=>$v){
$artilceName.=$v->articles->articles.',';
    }
}


                 echo rtrim($artilceName,','); ?>  </td>
         
              </tr>
               @endforeach
                </table>
                </div>
                <div>
                <td><a href="{{ url('add-blogger') }}">Add Blogger</a></td>
                
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
