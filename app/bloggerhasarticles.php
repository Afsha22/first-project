<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class bloggerhasarticles extends Model
{
    protected $table = 'bloggerhasarticle';
  
    
    public function articles(){
        return $this->belongsTo('App\articles', 'article_id');
    }
}
